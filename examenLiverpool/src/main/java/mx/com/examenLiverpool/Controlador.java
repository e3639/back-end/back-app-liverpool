package mx.com.examenLiverpool;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {

	
    @GetMapping("inicio")
	@ResponseBody
	 public JSONObject inicio(@RequestParam(value="name", required=true, defaultValue="gs1024904")String folio) {
		 
		 //System.out.println("folio:"+folio+"/n fecha y hora:"+fecha);
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		 System.out.println("folio:"+folio+"/n Fecha Actual-> "+dtf.format(LocalDateTime.now()));
		 
		 JSONObject obj = new JSONObject();
		 obj.put("folio:", folio);
	     //obj.put("fecha y hora:", dtf);
		 return obj;
		 
		 
	 }
}
